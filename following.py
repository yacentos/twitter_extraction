# -*- coding: utf-8 -*-
import time
import os
import threading

import re
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from _0_Functions import *


def tweets():
    # print lock.locked, " lock ", lock
    connection = connect_db()
    cursor = connection.cursor()

    sql_recherche = " select tweet_user_id ,  max(tweet_permalink_path) from tweets where tweet_user_id not in " \
                    " ( select user_id_initial  from following  ) group by tweet_user_id order by tweet_user_id ASC"

    cursor.execute(sql_recherche)
    tweets_items = cursor.fetchall()
    nbr_tweets = cursor.rowcount
    print nbr_tweets, "   ", sql_recherche
    count = 0
    nom = " Twitter "
    ids = []
    following_id_list = []
    if nbr_tweets > 0:
        driver = Acces_Webdriver()
        Authentification(driver, "", "")

    for tweet_item in tweets_items:
        user_id_initial = str(tweet_item[0])
        count += 1

        if user_id_initial not in ids:
            del following_id_list [:]
            ids.append(user_id_initial)
            user_link = str(tweet_item[1])

            # print " 1 tweet user link  " ,user_link
            user_link = user_link.split("/status")
            user_link = user_link[0]
            # print " 2 tweet user link  ", user_link


            try:

                url = 'https://twitter.com' + user_link + "/following"
                print nbr_tweets, "  ", count, "  ", url
                # try:
                driver.get(url)
                time.sleep(2)
                re_parcours = True
                nbr_quota = 150
                while re_parcours:
                    nbr_elements = scroll_down_follow(driver, "div[class='Grid-cell u-size1of2 u-lg-size1of3 u-mb10']",
                                                       nbr_quota)
                    if nbr_elements == 3:
                        re_parcours = False
                    print "-----------------------------------------------------------------------------"
                    nbr_quota = nbr_quota + 150
                    #scroll_down_follow(driver, "")

                    nbr_followings = 0
                    followings = driver.find_elements_by_css_selector("div[class='Grid-cell u-size1of2 u-lg-size1of3 u-mb10']")
                    for following in followings:
                        nbr_followings += 1
                        following_item_id = ""
                        following_type = ""
                        following_id = ""
                        following_href = ""
                        following_img_src = ""
                        following_statu = ""

                        following_more = following.find_elements_by_css_selector("div[class='js-stream-item']")
                        if len(following_more) > 0:
                            following_more = following_more[0]
                            following_item_id = following_more.get_attribute("data-item-id")
                            following_type = following_more.get_attribute("data-item-type")
                            following_id = following_more.get_attribute("id")

                        if following_id not in following_id_list:



                            following_more_href = following.find_elements_by_css_selector(
                                "a[class='ProfileCard-avatarLink js-nav js-tooltip']")
                            if len(following_more_href) > 0:
                                following_href = following_more_href[0]
                                following_href = following_href.get_attribute("href")

                            following_img = following.find_elements_by_css_selector(
                                "img[class='ProfileCard-avatarImage js-action-profile-avatar']")
                            if len(following_img) > 0:
                                following_img = following_img[0]
                                following_img_src = following_img.get_attribute("src")

                            following_status = following.find_elements_by_css_selector("span[class='FollowStatus']")
                            if len(following_status) > 0:
                                following_statu = following_status[0]
                                following_statu = following_statu.get_attribute("innerHTML")

                            # print nbr_followings, " __ ",following_item_id , " 1  ",following_type ,"   2  ", following_id,
                            # "  3  ",following_href,"  4  ",following_img_src,"   5  ",following_statu , "  6  "

                            ajouter_follower_following(cursor, connection, nbr_followings, "following", user_id_initial,
                                                       following_item_id,
                                                       following_type, following_id, following_href, following_statu, following_img_src)

                            # time.sleep(100)
                            following_id_list.append(following_id)
    

                time.sleep(5)

            except Exception as e:
                  PrintException()
            #        driver.close()
            #        driver.quit()


            # except Exception as e:
            # if "unexpectedly exited. Status code was" in e:
            #   exit()

    if nbr_tweets > 0:
        driver.close()
        driver.quit()


if __name__ == '__main__':
    tweets()
