# -*- coding: utf-8 -*-
import collections
import matplotlib.pylab as plt

from _0_Functions import *

def statistics():

    connection = connect_db()
    cursor = connection.cursor()
    cursor2 = connection.cursor()

    sql = "SELECT FROM_UNIXTIME(tweet_time_unix , '%Y') as ye , count(FROM_UNIXTIME(tweet_time_unix , '%Y')) AS annee_tweet " \
          "FROM tweets group by ye order by tweet_time_unix ASC;"
    stat_annees = statistics_tweets(sql, cursor2)
    stat_annees = collections.OrderedDict(sorted(stat_annees.items()))
    print " Statistics des annnes sont : ", stat_annees
    dessiner(stat_annees)

    for key in stat_annees.keys():
        print key
        sql = "SELECT FROM_UNIXTIME(tweet_time_unix , '%m') , count(FROM_UNIXTIME(tweet_time_unix , '%m')) AS annee_tweet " \
          "FROM tweets group by FROM_UNIXTIME(tweet_time_unix , '%m') where FROM_UNIXTIME(tweet_time_unix , '%Y') ='"+key+"' ;"
        stat_annees_detaille = statistics_tweets(sql, cursor2)
        dessiner(stat_annees_detaille,"title")

    exit(0)

    sql = "SELECT FROM_UNIXTIME(tweet_time_unix , '%m') , count(FROM_UNIXTIME(tweet_time_unix , '%m')) AS annee_tweet " \
          "FROM tweets group by FROM_UNIXTIME(tweet_time_unix , '%m') ;"
    stat_mois = statistics_tweets(sql, cursor2)
    stat_mois = collections.OrderedDict(sorted(stat_mois.items()))
    print " Statistics des mois sont : ", stat_mois
    dessiner(stat_mois)

    sql = "SELECT FROM_UNIXTIME(tweet_time_unix , '%d') , count(FROM_UNIXTIME(tweet_time_unix , '%d')) AS annee_tweet " \
          "FROM tweets group by FROM_UNIXTIME(tweet_time_unix , '%d') ;"
    stat_day = statistics_tweets(sql, cursor2)
    stat_day = collections.OrderedDict(sorted(stat_day.items()))
    print stat_day
    for ele in stat_day:
        print " Statistics des jours sont : ", ele,"  ",stat_day[ele]
    dessiner(stat_day)

    sql = " select tweet_user_id , tweet_retweets from tweets order by tweet_time_unix ASC"
    cursor.execute(sql)
    mes_tweets = cursor.fetchall()
    nbr_tweets = len(mes_tweets)
    print " nombre tweets : ", nbr_tweets

    # Idex of a tweet t
    index = 0
    exit(0)
    for tweet in mes_tweets:
        # A tweet Author
        author_id = tweet[0]

        #-------------------------------- Tweet Level--------------------------------------------------
        # Number of followers of a tweet t author
        n_followers = 0
        n_following = 0
        sql_n_followers = " select tweets_followers , tweets_abonnemenets from user_profile where user_id = " + str(author_id)
        cursor2.execute(sql_n_followers)
        results = cursor2.fetchone()
        n_followers = results[0]
        n_following = results[1]
        print " nombre Followers : ", n_followers

        extracted_n_followers = 0
        sql_extracted_n_followers = " select count(*) from followers where user_id_initial = " + str(author_id)
        cursor2.execute(sql_extracted_n_followers)
        results = cursor2.fetchone()
        extracted_n_followers = results[0]
        print " nombre Extracted Followers : ",n_followers, "  __  ", extracted_n_followers

        # Number of retweeters of a tweet t
        n_retweeters = 0
        n_retweeters = tweet[1]


        # -------------------------------- Fllower Level--------------------------------------------------
        n_extracted_following = 0
        sql_n_extracted_following = " select count(*) from followers where user_id_initial = " + str(author_id)
        cursor2.execute(sql_n_extracted_following)
        results = cursor2.fetchone()
        n_extracted_following = results[0]
        print " nombre EXtracted Following : ", n_following , "  ___  ", n_extracted_following



def statistics_tweets(sql,curs):

    curs.execute(sql)
    results = curs.fetchall()
    nbr_rows = curs.rowcount
    tab = {}
    for result in results:
        tab[result[0]] = result[1]
    return  tab


def dessiner(d,title):
    plt.bar(range(len(d)), d.values(), align="center")
    plt.xticks(range(len(d)), list(d.keys()))
    plt.show()


if __name__ == '__main__':
    statistics()