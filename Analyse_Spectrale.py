from igraph import Graph
import numpy as np
"""
import matplotlib.pylab as plt

def histo (x):
    hist, bin_edges = np.histogram (x, density=True)
    width = 0.7 * (bin_edges [1] - bin_edges [0])
    center = (bin_edges[:-1] + bin_edges[1:]) / 2
    plt.bar (center, hist, align='center', width=width)
    plt.show ()
"""

if __name__ == '__main__':
    karate = Graph.Read_GraphML ("karate.GraphML")
    karate.vs ["label"] = [str(j) for j in range(34)]

    L = Graph.laplacian (karate)
    valp, vecp = np.linalg.eig(L)
    idx = valp.argsort()[::-1]
    valp = valp[idx]
    vecp = vecp[:, idx]

    print valp
    print vecp


