# -*- coding: utf-8 -*-
import time
import os
import threading

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from _0_Functions import *


def tweets(nom_table, champ_to_update, champ_where):
    # print lock.locked, " lock ", lock
    connection = connect_db()
    cursor = connection.cursor()

    sql_recherche = " select " + str(champ_where) + " ,user_id  from " + str(nom_table) + " , user_profile " \
                                                                                          "where " + str(
        champ_where) + "=user_link  and reactor_id=-1 group by " + str(champ_where) + " "

    cursor.execute(sql_recherche)
    tweets_items = cursor.fetchall()
    nbr_tweets = cursor.rowcount
    print nbr_tweets, "   ", sql_recherche
    for tweet_item in tweets_items:
        reactor_screen_name = tweet_item[0]
        rtweeter_id = tweet_item[1]
        sql1 = " update " + str(nom_table) + " set " + str(champ_to_update) + "=" + str(rtweeter_id) + " " \
                "where " + str( champ_where) + " = '" + str(reactor_screen_name) + "' "
        print " sql1 ", sql1
        cursor.execute(sql1)
        connection.commit()

    sql_recherche = " select " + str(champ_where) + "  from " + str(nom_table) + "  " \
                                                                                 "where " + str(
        champ_where) + " not in " \
                       " ( select user_link  from user_profile ) group by " + str(champ_where) + " "
    cursor.execute(sql_recherche)
    tweets_items = cursor.fetchall()
    nbr_tweets = cursor.rowcount
    print nbr_tweets, "   ", sql_recherche
    count = 0
    nom = " Twitter "
    ids = []
    driver = Acces_Webdriver()

    for tweet_item in tweets_items:
        retweeter_screen_name = str(tweet_item[0])
        count += 1
        # if tweet_user_id not in ids:
        # ids.append(tweet_user_id)
        # print " 1 tweet user link  " ,user_link
        # print " 2 tweet user link  ", user_link


        try:

            url = 'https://twitter.com' + retweeter_screen_name
            print nbr_tweets, "  ", count, "  ", url
            try:
                driver.get(url)
                time.sleep(2)
                tweet_user_id = ''
                tweet_user_ids = driver.find_elements_by_css_selector("div[class='ProfileNav']")
                print "  tweet_user_id  ", tweet_user_id
                if len(tweet_user_ids) > 0:
                    tweet_user_id = tweet_user_ids[0]
                    tweet_user_id = tweet_user_id.get_attribute("data-user-id")
                    if tweet_user_id == None:
                        break

                ""
                tweets_elem_nbr_item = -1
                tweets_nbr = driver.find_elements_by_css_selector(
                    "li[class='ProfileNav-item ProfileNav-item--tweets is-active']")
                if len(tweets_nbr) > 0:
                    tweets_elem_nbr = tweets_nbr[0]
                    tweets_elem_nbr = tweets_elem_nbr.find_element_by_css_selector("span[class='ProfileNav-value']")
                    tweets_elem_nbr_item = tweets_elem_nbr.get_attribute("data-count")
                    if tweets_elem_nbr_item == None:
                        tweets_elem_nbr_item = -1
                        # print " nbr tweets " , tweets_elem_nbr_item

                j_aime_item = -1
                tweets_j_aimes = driver.find_elements_by_css_selector(
                    "li[class='ProfileNav-item ProfileNav-item--favorites']")
                if len(tweets_j_aimes) > 0:
                    tweets_j_aimes = tweets_j_aimes[0]
                    j_aime_elem = tweets_j_aimes.find_element_by_css_selector("span[class='ProfileNav-value']")
                    j_aime_item = j_aime_elem.get_attribute("data-count")
                    if j_aime_item == None:
                        j_aime_item = -1
                        # print " total  j aimes ", j_aime_item

                tweets_moments_item = -1
                tweets_moments = driver.find_elements_by_css_selector(
                    "li[class='ProfileNav-item ProfileNav-item--moments']")
                if len(tweets_moments) > 0:
                    tweets_moments = tweets_moments[0]
                    tweets_moments = tweets_moments.find_element_by_css_selector("span[class='ProfileNav-value']")
                    tweets_moments_item = tweets_moments.get_attribute("data-count")
                    if tweets_moments_item == None:
                        tweets_moments_item = -1
                        # print " tweets_moments ", tweets_moments_item

                abonnemenet_item = -1
                tweets_abonnemenets = driver.find_elements_by_css_selector(
                    "li[class='ProfileNav-item ProfileNav-item--following']")
                if len(tweets_abonnemenets) > 0:
                    tweets_abonnemenets = tweets_abonnemenets[0]
                    abonnemenets = tweets_abonnemenets.find_element_by_css_selector("span[class='ProfileNav-value']")
                    abonnemenet_item = abonnemenets.get_attribute("data-count")
                    if abonnemenet_item == None:
                        abonnemenet_item = -1
                        # print " total  abonnemenet ", abonnemenet_item

                follower_item = -1
                tweets_followers = driver.find_elements_by_css_selector(
                    "li[class='ProfileNav-item ProfileNav-item--followers']")
                if len(tweets_followers) > 0:
                    tweets_followers = tweets_followers[0]
                    follower = tweets_followers.find_element_by_css_selector("span[class='ProfileNav-value']")
                    follower_item = follower.get_attribute("data-count")
                    if follower_item == None:
                        follower_item = -1
                        # print " total  abonnee ", follower_item

                tweets_list_item = -1
                tweets_lists = driver.find_elements_by_css_selector(
                    "li[class='ProfileNav-item ProfileNav-item--lists']")
                if len(tweets_lists) > 0:
                    tweets_lists = tweets_lists[0]
                    tweets_list = tweets_lists.find_element_by_css_selector("span[class='ProfileNav-value']")
                    tweets_list_item = tweets_list.get_attribute("data-count")
                    if tweets_list_item == None:
                        tweets_list_item = -1
                        # print " total  abonnemenet ", tweets_list_item

                tweets_location_item = 'null'
                tweets_locations = driver.find_elements_by_css_selector("div[class='ProfileHeaderCard-location ']")
                if len(tweets_locations) > 0:
                    tweets_location = tweets_locations[0]
                    tweets_location = tweets_location.find_element_by_css_selector(
                        "span[class='ProfileHeaderCard-locationText u-dir']")
                    tweets_location_item = tweets_location.get_attribute("innerHTML")
                    tweets_location_item = re.sub(' +', ' ', tweets_location_item)
                    tweets_location_item = tweets_location_item.replace("\n", "")
                    tweets_location_item = tweets_location_item.replace("'", "")
                    # print " total  tweets_location ", tweets_location_item

                tweets_joinDate_item = 'null'
                tweets_joinDate = driver.find_elements_by_css_selector("div[class='ProfileHeaderCard-joinDate']")
                if len(tweets_joinDate) > 0:
                    tweets_joinDate = tweets_joinDate[0]
                    tweets_joinDate = tweets_joinDate.find_element_by_css_selector(
                        "span[class='ProfileHeaderCard-joinDateText js-tooltip u-dir']")
                    tweets_joinDate_item = tweets_joinDate.get_attribute("data-original-title")

                    # print " total  tweets_joinDate ", tweets_joinDate_item

                url_page_item = 'null'
                url_page = driver.find_elements_by_css_selector("div[class='ProfileHeaderCard-url ']")
                if len(url_page) > 0:
                    url_page = url_page[0]
                    url_page = url_page.find_element_by_css_selector("a[class='u-textUserColor']")
                    url_page_item = url_page.get_attribute("title")
                    # print " profile  url_page ", url_page_item

                photo_rail_item = 'null'
                photo_rail = driver.find_elements_by_css_selector("div[class='PhotoRail-heading']")
                if len(photo_rail) > 0:
                    photo_rail = photo_rail[0]
                    photo_rail = photo_rail.find_element_by_css_selector("a[class='PhotoRail-headingWithCount js-nav']")
                    photo_rail_item = photo_rail.get_attribute("innerHTML")
                    photo_rail_item = re.sub(' +', ' ', photo_rail_item)
                    photo_rail_item = photo_rail_item.replace("\n", "")
                    # print " profile  photo_rail ", photo_rail_item

                birthday_item = 'null'
                birthday = driver.find_elements_by_css_selector("div[class='ProfileHeaderCard-birthdate']")
                if len(birthday) > 0:
                    birthday = birthday[0]
                    birthday = birthday.find_element_by_css_selector("span[class='js-tooltip']")
                    birthday_item = birthday.get_attribute("innerHTML")
                    # print " profile  birthday ", birthday_item

                ajouter_tweet_user(cursor, connection, count, nom, tweet_user_id, retweeter_screen_name,
                                   tweets_elem_nbr_item, j_aime_item, tweets_moments_item,
                                   abonnemenet_item, follower_item, tweets_list_item, tweets_location_item,
                                   tweets_joinDate_item,
                                   url_page_item, photo_rail_item, birthday_item)

                update_tweeter_user(cursor, connection, count, nom_table, champ_to_update, tweet_user_id, champ_where,
                                    retweeter_screen_name)
                time.sleep(5)

            except Exception as e:
                PrintException()
                driver.close()
                driver.quit()


        except Exception as e:
            PrintException()

    driver.close()
    driver.quit()


if __name__ == '__main__':
    tweets("tweets_retweets", "reactor_id", "reactor_screen_name")
    tweets("tweets_likes", "reactor_id", "reactor_screen_name")
