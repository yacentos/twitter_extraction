# -*- coding: utf-8 -*-
import time
import os
import threading
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from _0_Functions import *


def tweets():
    # print lock.locked, " lock ", lock
    connection = connect_db()
    cursor = connection.cursor()

    sql = " select tweet_permalink_path , tweet_tweet_id , tweet_user_id , tweet_time_unix  from tweets" \
          " where tweet_retweets > 0 and parcouru = 0 order by tweet_user_id ASC "

    cursor.execute(sql)
    mes_tweets = cursor.fetchall()
    nbr_tweets = cursor.rowcount
    lock = threading.Lock()
    print " nombre tweets : ", nbr_tweets

    if  nbr_tweets > 0 :
        driver = Acces_Webdriver()
        Authentification(driver,"","")
    time.sleep(10)
    for cher in mes_tweets:
        print cher
        debut = 1
        try:
            # "/home/yacentos/Bureau/facebook_Extraction_05_05_2017/Extraction/chromedriver"
            # D:\\chromedriver.exe
            url = 'https://twitter.com'
            url = url + cher[0]
            print "url  : ", url
            try:

                driver.get(url)
                time.sleep(5)

                # print src

                elem = driver.find_element_by_css_selector("div[class='IconContainer js-tooltip'][title='Retweeter']")
                print " on va cliquer "
                # elem.clear()
                # elem.send_keys(unicode(cher, errors='replace'))
                # time.sleep(5)
                # elem = driver.find_element_by_id("search_submit")
                elem.click()
                scroll_down_general(driver, '<li class="js-stream-item')

                # driver.sendKeys(Keys.PAGE_DOWN)
                time.sleep(7)
                src = driver.page_source

                tweets_elem = src.split('<li class="js-stream-item')
                # tweets_elem = tweets_elem[1]
                print " Nombre tweets ", len(tweets_elem)
                i = 0
                tweet_tweet_id = ''
                tweet_item_id = ''
                tweet_permalink_path = ''
                tweet_name = ''
                tweet_user_id = ''
                tweet_text = ''
                tweet_tweet_type = ''
                tweet_component_context = ''
                tweet_card_type = ''
                tweet_has_card = 'false'
                tweet_time_title = ''
                tweet_time_unix = ''
                tweet_has_parent = 'false'
                tweet_conversation_id = ''
                tweet_nonce_nonce = ''
                tweet_stat_initialized = ''
                tweet_conversation_section = ''
                tweet_disclosure_type = ''

                for t in tweets_elem:
                    # if i > 0 :
                    # print ' 0 here ' , t
                    tweet = t.split('data-disclosure-type="', 1)
                    if (len(tweet) > 1):
                        tweet_disclosure_type = tweet[1].split('"', 1)
                        tweet_disclosure_type = tweet_disclosure_type[0]

                    tweet = t.split('data-tweet-nonce="', 1)
                    if (len(tweet) > 1):
                        tweet_nonce_nonce = tweet[1].split('"', 1)
                        tweet_nonce_nonce = tweet_nonce_nonce[0]

                    tweet = t.split('data-tweet-stat-initialized="', 1)
                    if (len(tweet) > 1):
                        tweet_stat_initialized = tweet[1].split('"', 1)
                        tweet_stat_initialized = tweet_stat_initialized[0]

                    tweet = t.split('data-conversation-section-quality="', 1)
                    if (len(tweet) > 1):
                        tweet_conversation_section = tweet[1].split('"', 1)
                        tweet_conversation_section = tweet_conversation_section[0]

                    tweet = t.split('data-has-parent-tweet="', 1)
                    if (len(tweet) > 1):
                        tweet_has_parent = tweet[1].split('"', 1)
                        tweet_has_parent = tweet_has_parent[0]

                    tweet = t.split('data-conversation-id="', 1)
                    if (len(tweet) > 1):
                        tweet_conversation_id = tweet[1].split('"', 1)
                        tweet_conversation_id = tweet_conversation_id[0]

                    tweet = t.split('data-tweet-id="', 1)
                    if (len(tweet) > 1):
                        tweet_tweet_id = tweet[1].split('"', 1)
                        tweet_tweet_id = tweet_tweet_id[0]
                        # print ' 1 here '
                    tweet = t.split('data-item-id="', 1)
                    if (len(tweet) > 1):
                        tweet_item_id = tweet[1].split('"', 1)
                        tweet_item_id = tweet_item_id[0]
                        # print ' 2 here '
                    tweet = t.split('data-permalink-path="', 1)
                    if (len(tweet) > 1):
                        tweet_permalink_path = tweet[1].split('"', 1)
                        tweet_permalink_path = tweet_permalink_path[0]
                        # print ' 3 here '
                    tweet = t.split('data-name="', 1)
                    if (len(tweet) > 1):
                        tweet_name = tweet[1].split('"', 1)
                        tweet_name = tweet_name[0]
                        # print ' 4 here'
                    tweet = t.split('data-user-id="', 1)
                    if (len(tweet) > 1):
                        tweet_user_id = tweet[1].split('"', 1)
                        tweet_user_id = tweet_user_id[0]
                        # print ' 5 here'
                    tweet = t.split('data-item-type="', 1)
                    if (len(tweet) > 1):
                        tweet_tweet_type = tweet[1].split('"', 1)
                        tweet_tweet_type = tweet_tweet_type[0]
                        # print ' 6 here'
                    tweet = t.split('data-mentions="', 1)
                    # print len(tweet) , ' 6 here' , tweet
                    if (len(tweet) > 1):
                        tweet_tweet_mentions = tweet[1].split('"', 1)
                        tweet_tweet_mentions = tweet_tweet_mentions[0]
                        # print ' 7 here '
                    tweet = t.split('data-card2-type="', 1)
                    if (len(tweet) > 1):
                        tweet_card_type = tweet[1].split('"', 1)
                        tweet_card_type = tweet_card_type[0]

                    tweet = t.split('data-has-cards="', 1)
                    if (len(tweet) > 1):
                        tweet_has_card = tweet[1].split('"', 1)
                        tweet_has_card = tweet_has_card[0]

                    tweet = t.split('data-component-context="', 1)
                    if (len(tweet) > 1):
                        tweet_component_context = tweet[1].split('"', 1)
                        tweet_component_context = tweet_component_context[0]

                    tweet = t.split('js-permalink js-nav js-tooltip" title="', 1)
                    if (len(tweet) > 1):
                        tweet_time_title = tweet[1].split('"', 1)
                        tweet_time_title = tweet_time_title[0]

                    tweet = t.split('data-time="', 1)
                    if (len(tweet) > 1):
                        tweet_time_unix = tweet[1].split('"', 1)
                        tweet_time_unix = tweet_time_unix[0]
                        # print ' here '
                    tweet = t.split('<p class="TweetTextSize', 1)
                    if (len(tweet) > 1):
                        tweet_text = tweet[1].split('</p>', 1)
                        tweet_text = tweet_text[0]

                    tweet_response = 0
                    tweet_retweets = 0
                    tweet_j_aime = 0

                    statistique = t.split('<span class="ProfileTweet-action--retweet u-hiddenVisually">')
                    statistique_reponse = statistique[0]

                    recher1 = '<span class="ProfileTweet-actionCount" data-tweet-stat-count="'
                    recher2 = '<span class="ProfileTweet-actionCount" aria-hidden="true" data-tweet-stat-count="'
                    if recher1 in statistique_reponse:
                        tweet = statistique_reponse.split(recher1, 1)
                    else:
                        tweet = statistique_reponse.split(recher2, 1)

                    if (len(tweet) > 1):
                        tweet_response = tweet[1].split('"', 1)
                        tweet_response = tweet_response[0]

                    statistique_retweet_j_aime = statistique[1].split(
                        '<span class="ProfileTweet-action--favorite u-hiddenVisually">')

                    if recher1 in statistique_retweet_j_aime[0]:
                        tweet = statistique_retweet_j_aime[0].split(recher1, 1)
                    else:
                        tweet = statistique_retweet_j_aime[0].split(recher2, 1)

                    if (len(tweet) > 1):
                        tweet_retweets = tweet[1].split('"', 1)
                        tweet_retweets = tweet_retweets[0]

                    if recher1 in statistique_retweet_j_aime[1]:
                        tweet = statistique_retweet_j_aime[1].split(recher1, 1)
                    else:
                        tweet = statistique_retweet_j_aime[1].split(recher2, 1)

                    if (len(tweet) > 1):
                        tweet_j_aime = tweet[1].split('"', 1)
                        tweet_j_aime = tweet_j_aime[0]
                    """
                    print " Tweet (",i,") tweet_tweet_id : ",tweet_tweet_id," , tweet_item_id : ",tweet_item_id\
                        , " , tweet_permalink_path : ", tweet_permalink_path," , tweet name : ",tweet_name,\
                        " , tweet user id : ",tweet_user_id , " , tweet_tweet_type : ", tweet_tweet_type, \
                        " , tweet card type  : ", tweet_card_type, " , tweet_has_card : ", tweet_has_card,\
                        " , tweet_component_context : ", tweet_component_context, " , tweet_time_title : ", tweet_time_title,\
                        " , tweet_text : ", tweet_text, " tweet_time_unix :", tweet_time_unix,\
                        " , tweet_response : ", tweet_response," , tweet_retweets : ", tweet_retweets," , tweet_j_aime : ", tweet_j_aime
                    """
                    ajouter_tweet_share(cursor, connection, i, "tweets_share", tweet_tweet_id, tweet_item_id, tweet_user_id,
                                        tweet_tweet_type,
                                        tweet_name, tweet_permalink_path, tweet_has_card, tweet_card_type,
                                        tweet_component_context,
                                        tweet_time_title, tweet_text, tweet_response, tweet_retweets, tweet_j_aime,
                                        tweet_time_unix,
                                        tweet_disclosure_type, tweet_nonce_nonce, tweet_stat_initialized,
                                        tweet_conversation_section,
                                        tweet_conversation_id, tweet_has_parent, cher[1])

                    connection.commit()
                    # i += 1

                sql2 = " update tweets set parcouru = 1 where tweet_tweet_id=" + str(
                    cher[1]) + " and tweet_user_id = " + str(cher[2]) + "" \
                                                                        " and tweet_time_unix = " + str(cher[3]) + " "
                print " sql 2 : ", sql2
                # cursor.execute(sql2)
                # connection.commit()

                # driver.close()
                # driver.quit()

                time_to_sleep = 15
                print " tiiiiiiiiiiiii me ", time_to_sleep

                time.sleep(time_to_sleep)

            except Exception as e:
                PrintException()
                driver.close()
                driver.quit()


        except Exception as e:
            PrintException()
    driver.close()
    driver.quit()


if __name__ == '__main__':
    tweets()
