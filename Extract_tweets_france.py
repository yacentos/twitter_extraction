# -*- coding: utf-8 -*-
import time
import os
import threading
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from _0_Functions import *


def tweets():
    # print lock.locked, " lock ", lock
    connection = connect_db()  # _db("environnement_tweeter_france")
    cursor = connection.cursor()

    debut = 0
    token = 1
    List_cherch = ["greenfrance", "cop_21", "كوب_21", "قمة_باريس_للمناخ", "اتفاق_باريس_للمناخ"]

    # List_cherch = [ "كوب_21"]
    lock = threading.Lock()
    file_object = open("statistique_france.txt", "w")
    tweet_tweet_id_list = []

    for cher in List_cherch:
        line = cher + " : "
        del tweet_tweet_id_list[:]
        print cher
        debut = 1
        try:
            # "/home/yacentos/Bureau/facebook_Extraction_05_05_2017/Extraction/chromedriver"
            # D:\\chromedriver.exe
            os.environ["webdriver.chrome.driver"] = chromedriver
            print "1   ", chromedriver
            try:
                driver = webdriver.Chrome(chromedriver)
            except Exception as e:
                PrintException()
            print "2   ", driver
            url = 'https://developer.twitter.com/Streaming-API-Documentation#statuses/sample'
            try:
                driver.get(url)
                time.sleep(3)
                src = driver.page_source
                # print src

                elem = driver.find_element_by_id("search_q")
                elem.clear()
                elem.send_keys(unicode(cher, errors='replace'))
                time.sleep(5)
                elem = driver.find_element_by_id("search_submit")
                elem.send_keys(Keys.RETURN)
                time.sleep(7)
                re_parcours = True
                nbr_quota = 150
                while re_parcours:
                    nbr_elements = scroll_down_general(driver, '<li class="js-stream-item', nbr_quota)
                    if nbr_elements == 3:
                        re_parcours = False
                    print "-----------------------------------------------------------------------------"
                    nbr_quota = nbr_quota + 150

                    src = driver.page_source

                    tweets_elem = src.split('<li class="js-stream-item')
                    # tweets_elem = tweets_elem[1]
                    print " Nombre tweets ", len(tweets_elem)
                    line = line + str(len(tweets_elem))
                    file_object.write(line)
                    i = 0
                    tweet_tweet_id = ''
                    tweet_item_id = ''
                    tweet_permalink_path = ''
                    tweet_name = ''
                    tweet_user_id = ''
                    tweet_text = ''
                    tweet_tweet_type = ''
                    tweet_component_context = ''
                    tweet_card_type = ''
                    tweet_has_card = 'false'
                    tweet_time_title = ''
                    tweet_time_unix = ''
                    tweet_has_parent = 'false'
                    tweet_conversation_id = ''
                    tweet_nonce_nonce = ''
                    tweet_stat_initialized = ''
                    tweet_conversation_section = ''
                    tweet_disclosure_type = ''

                    for t in tweets_elem:
                        if i > 0:
                            # print ' 0 here ' , t

                            tweet = t.split('data-tweet-id="', 1)
                            if (len(tweet) > 1):
                                tweet_tweet_id = tweet[1].split('"', 1)
                                tweet_tweet_id = tweet_tweet_id[0]

                            if tweet_tweet_id not in tweet_tweet_id_list:
                                tweet = t.split('data-disclosure-type="', 1)
                                if (len(tweet) > 1):
                                    tweet_disclosure_type = tweet[1].split('"', 1)
                                    tweet_disclosure_type = tweet_disclosure_type[0]

                                tweet = t.split('data-tweet-nonce="', 1)
                                if (len(tweet) > 1):
                                    tweet_nonce_nonce = tweet[1].split('"', 1)
                                    tweet_nonce_nonce = tweet_nonce_nonce[0]

                                tweet = t.split('data-tweet-stat-initialized="', 1)
                                if (len(tweet) > 1):
                                    tweet_stat_initialized = tweet[1].split('"', 1)
                                    tweet_stat_initialized = tweet_stat_initialized[0]

                                tweet = t.split('data-conversation-section-quality="', 1)
                                if (len(tweet) > 1):
                                    tweet_conversation_section = tweet[1].split('"', 1)
                                    tweet_conversation_section = tweet_conversation_section[0]

                                tweet = t.split('data-has-parent-tweet="', 1)
                                if (len(tweet) > 1):
                                    tweet_has_parent = tweet[1].split('"', 1)
                                    tweet_has_parent = tweet_has_parent[0]

                                tweet = t.split('data-conversation-id="', 1)
                                if (len(tweet) > 1):
                                    tweet_conversation_id = tweet[1].split('"', 1)
                                    tweet_conversation_id = tweet_conversation_id[0]

                                # print ' 1 here '
                                tweet = t.split('data-item-id="', 1)
                                if (len(tweet) > 1):
                                    tweet_item_id = tweet[1].split('"', 1)
                                    tweet_item_id = tweet_item_id[0]
                                # print ' 2 here '
                                tweet = t.split('data-permalink-path="', 1)
                                if (len(tweet) > 1):
                                    tweet_permalink_path = tweet[1].split('"', 1)
                                    tweet_permalink_path = tweet_permalink_path[0]
                                # print ' 3 here '
                                tweet = t.split('data-name="', 1)
                                if (len(tweet) > 1):
                                    tweet_name = tweet[1].split('"', 1)
                                    tweet_name = tweet_name[0]
                                # print ' 4 here'
                                tweet = t.split('data-user-id="', 1)
                                if (len(tweet) > 1):
                                    tweet_user_id = tweet[1].split('"', 1)
                                    tweet_user_id = tweet_user_id[0]
                                # print ' 5 here'
                                tweet = t.split('data-item-type="', 1)
                                if (len(tweet) > 1):
                                    tweet_tweet_type = tweet[1].split('"', 1)
                                    tweet_tweet_type = tweet_tweet_type[0]
                                # print ' 6 here'
                                tweet = t.split('data-mentions="', 1)
                                # print len(tweet) , ' 6 here' , tweet
                                if (len(tweet) > 1):
                                    tweet_tweet_mentions = tweet[1].split('"', 1)
                                    tweet_tweet_mentions = tweet_tweet_mentions[0]
                                # print ' 7 here '
                                tweet = t.split('data-card2-type="', 1)
                                if (len(tweet) > 1):
                                    tweet_card_type = tweet[1].split('"', 1)
                                    tweet_card_type = tweet_card_type[0]

                                tweet = t.split('data-has-cards="', 1)
                                if (len(tweet) > 1):
                                    tweet_has_card = tweet[1].split('"', 1)
                                    tweet_has_card = tweet_has_card[0]

                                tweet = t.split('data-component-context="', 1)
                                if (len(tweet) > 1):
                                    tweet_component_context = tweet[1].split('"', 1)
                                    tweet_component_context = tweet_component_context[0]

                                tweet = t.split('js-permalink js-nav js-tooltip" title="', 1)
                                if (len(tweet) > 1):
                                    tweet_time_title = tweet[1].split('"', 1)
                                    tweet_time_title = tweet_time_title[0]

                                tweet = t.split('data-time="', 1)
                                if (len(tweet) > 1):
                                    tweet_time_unix = tweet[1].split('"', 1)
                                    tweet_time_unix = tweet_time_unix[0]
                                # print ' here '
                                tweet = t.split('<p class="TweetTextSize', 1)
                                if (len(tweet) > 1):
                                    tweet_text = tweet[1].split('</p>', 1)
                                    tweet_text = tweet_text[0]

                                tweet_response = 0
                                tweet_retweets = 0
                                tweet_j_aime = 0

                                statistique = t.split('<span class="ProfileTweet-action--retweet u-hiddenVisually">')
                                statistique_reponse = statistique[0]

                                recher1 = '<span class="ProfileTweet-actionCount" data-tweet-stat-count="'
                                recher2 = '<span class="ProfileTweet-actionCount" aria-hidden="true" data-tweet-stat-count="'
                                if recher1 in statistique_reponse:
                                    tweet = statistique_reponse.split(recher1, 1)
                                else:
                                    tweet = statistique_reponse.split(recher2, 1)

                                if (len(tweet) > 1):
                                    tweet_response = tweet[1].split('"', 1)
                                    tweet_response = tweet_response[0]

                                statistique_retweet_j_aime = statistique[1].split(
                                    '<span class="ProfileTweet-action--favorite u-hiddenVisually">')

                                if recher1 in statistique_retweet_j_aime[0]:
                                    tweet = statistique_retweet_j_aime[0].split(recher1, 1)
                                else:
                                    tweet = statistique_retweet_j_aime[0].split(recher2, 1)

                                if (len(tweet) > 1):
                                    tweet_retweets = tweet[1].split('"', 1)
                                    tweet_retweets = tweet_retweets[0]

                                if recher1 in statistique_retweet_j_aime[1]:
                                    tweet = statistique_retweet_j_aime[1].split(recher1, 1)
                                else:
                                    tweet = statistique_retweet_j_aime[1].split(recher2, 1)

                                if (len(tweet) > 1):
                                    tweet_j_aime = tweet[1].split('"', 1)
                                    tweet_j_aime = tweet_j_aime[0]
                                """
                                print " Tweet (",i,") tweet_tweet_id : ",tweet_tweet_id," , tweet_item_id : ",tweet_item_id\
                                    , " , tweet_permalink_path : ", tweet_permalink_path," , tweet name : ",tweet_name,\
                                    " , tweet user id : ",tweet_user_id , " , tweet_tweet_type : ", tweet_tweet_type, \
                                    " , tweet card type  : ", tweet_card_type, " , tweet_has_card : ", tweet_has_card,\
                                    " , tweet_component_context : ", tweet_component_context, " , tweet_time_title : ", tweet_time_title,\
                                    " , tweet_text : ", tweet_text, " tweet_time_unix :", tweet_time_unix,\
                                    " , tweet_response : ", tweet_response," , tweet_retweets : ", tweet_retweets," , tweet_j_aime : ", tweet_j_aime
                                """
                                ajouter_tweet(cursor, connection, i, "tweet", tweet_tweet_id, tweet_item_id, tweet_user_id,
                                              tweet_tweet_type,
                                              tweet_name, tweet_permalink_path, tweet_has_card, tweet_card_type,
                                              tweet_component_context,
                                              tweet_time_title, tweet_text, tweet_response, tweet_retweets, tweet_j_aime,
                                              tweet_time_unix,
                                              tweet_disclosure_type, tweet_nonce_nonce, tweet_stat_initialized,
                                              tweet_conversation_section,
                                              tweet_conversation_id, tweet_has_parent, "France", cher)

                                connection.commit()
                                tweet_tweet_id_list.append(tweet_tweet_id)
                        i += 1

                driver.close()
                driver.quit()

                time_to_sleep = 60
                print " tiiiiiiiiiiiii me ", time_to_sleep

                time.sleep(time_to_sleep)

            except Exception as e:
                PrintException()
                driver.close()
                driver.quit()


        except Exception as e:
            PrintException()
    file_object.close()


if __name__ == '__main__':
    tweets()
