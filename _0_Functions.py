# -*- coding: utf-8 -*-

import calendar
import datetime
import datetime as dt
import json
import linecache
import mysql.connector
import os
import re
import pickle
import time
import urllib2
import threading
import facebook
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

import sys
reload(sys)
sys.setdefaultencoding('utf-8')

chromedriver = "D:\chromedriver.exe"
database_name = "environnement_tweeter_maroc"  # "maroc36"  # bi3_chri facebook_share   maroc36
database_password = 'Bismillah@9'
is_paramtrable = 0
mot_cle = ''
parametre_group = ''

#mot_cle = 'and hs.hashtag_name like "%سنطرال%" or hs.hashtag_name like "%خليه%" or hs.hashtag_name like "%افريقيا%" or hs.hashtag_name like "%مازوط%") '
#mot_cle = 'and hs.hashtag_name like "%النكبة%" or hs.hashtag_name like "%مليونية_العودة‬%" or hs.hashtag_name like "%يوم_العبور%" or hs.hashtag_name like "%جمعة_النذير%") '

usr_pwd = []
# "/home/yacentos/parametres.txt"
# "D:\Token_Facebook\parametres.txt"



#print parametre_group, "|  :  ", database_name, "  :  ", database_password, "  :   ", usr_pwd

nombre_arret = 100000000000000000450
Date_D_arret = 1483228800  # 01/01/2017


def connect_db_db(db):
    print "  Connect to Mysql db  =>  ", db
    return mysql.connector.connect(user='root', password=database_password, host='localhost', database=db,
                                   charset='utf8mb4', use_unicode=True)


def connect_db():
    print "  Connect to Mysql db  =>  ", database_name
    return mysql.connector.connect(user='root', password=database_password, host='localhost', database=database_name,
                                   charset='utf8mb4', use_unicode=True)

def Acces_Webdriver():

    os.environ["webdriver.chrome.driver"] = chromedriver
    chrome_options = webdriver.ChromeOptions()
    prefs = {"profile.default_content_setting_values.notifications": 2}
    chrome_options.add_experimental_option("prefs", prefs)
    driver = webdriver.Chrome(chromedriver, chrome_options=chrome_options)
    return driver

def Authentification(driver, usr, pwd):
    url = 'https://twitter.com/login'
    driver.get(url)
    time.sleep(2)
    someElement = driver.find_element_by_css_selector(
        "input[class='js-username-field email-input js-initial-focus']")  # "input[type='text'][name='session[username_or_email]']")
    # someElement = driver.find_element_by_css_selector("input[class='rn-30o5oe rn-e84r5y rn-f1w8kp rn-cqzzvf rn-1hd4d43 rn-u0ci3n rn-13yce4e rn-fnigne rn-ndvcnb rn-gxnn5r rn-deolkf rn-homxoj rn-poiln3 rn-7cikom rn-156q2ks rn-15d164r rn-m611by rn-1qfoi16 rn-1mi0q7o rn-1hfyk0a rn-1lrr6ok rn-1dz5y72 rn-1ttztb7 rn-13qz1uu']")#"input[type='text'][name='session[username_or_email]']")
    someElement.send_keys("yassine.Elmoudene@gmail.Com")

    someElement = driver.find_element_by_css_selector(
        "input[class='js-password-field']")  # "input[type='text'][name='session[username_or_email]']")
    # someElement = driver.find_element_by_css_selector("input[class='rn-30o5oe rn-e84r5y rn-f1w8kp rn-cqzzvf rn-1hd4d43 rn-u0ci3n rn-13yce4e rn-fnigne rn-ndvcnb rn-gxnn5r rn-deolkf rn-homxoj rn-poiln3 rn-7cikom rn-156q2ks rn-15d164r rn-m611by rn-1qfoi16 rn-1mi0q7o rn-1hfyk0a rn-1lrr6ok rn-1dz5y72 rn-1ttztb7 rn-13qz1uu']")  # "input[type='text'][name='session[username_or_email]']")
    someElement.send_keys("Y@centosDHSDK/*-9")
    someElement = driver.find_element_by_css_selector(
        "button[class='submit EdgeButton EdgeButton--primary EdgeButtom--medium']")
    someElement.click()

def scroll_down_general(driver , selector,nbr_quota):

    SCROLL_PAUSE_TIME = 6
    count = 0

    # Get scroll height
    last_height = driver.execute_script("return document.body.scrollHeight")
    src = driver.page_source
    tweets_elem = src.split(selector)
    nbr_tweets_1 = 0
    nbr_tweets_2 = len(tweets_elem)
    print nbr_tweets_1," Nombre tweets ", nbr_tweets_2
    nbr_repetition = 0

    while (nbr_repetition < 3) :
        nbr_tweets_1 = nbr_tweets_2
        # Scroll down to bottom
        count += 1
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        # Wait to load page
        time.sleep(SCROLL_PAUSE_TIME)

        src = driver.page_source
        tweets_elem = src.split(selector)
        nbr_tweets_2 = len(tweets_elem)
        print nbr_repetition, "    _   ", nbr_tweets_1 , " Nombre tweets ", nbr_tweets_2 , "   ",SCROLL_PAUSE_TIME

        if nbr_tweets_2 == 0 :
            nbr_repetition = 3
            break
        elif nbr_tweets_2 > nbr_quota :
            nbr_repetition = 10
            break
        elif nbr_tweets_1 == nbr_tweets_2:
            nbr_repetition += 1
            SCROLL_PAUSE_TIME = SCROLL_PAUSE_TIME + 4



    return nbr_repetition

def PrintException():
    exc_type, exc_obj, tb = sys.exc_info()
    f = tb.tb_frame
    lineno = tb.tb_lineno
    filename = f.f_code.co_filename
    linecache.checkcache(filename)
    line = linecache.getline(filename, lineno, f.f_globals)
    print 'EXCEPTION IN ({}, LINE {} "{}"): {}'.format(filename, lineno, line.strip(), exc_obj)


def scroll_down_follow(driver,selector,nbr_quota):

    SCROLL_PAUSE_TIME = 4
    count = 0

    # Get scroll height
    last_height = driver.execute_script("return document.body.scrollHeight")
    src = driver.page_source
    tweets_elem = driver.find_elements_by_css_selector(selector)
    nbr_tweets_1 = 0
    nbr_tweets_2 = len(tweets_elem)
    print nbr_tweets_1," Nombre tweets ", nbr_tweets_2
    nbr_repetition = 0

    while (nbr_repetition < 3):
        nbr_tweets_1 = nbr_tweets_2
        # Scroll down to bottom
        count += 1
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        # Wait to load page
        time.sleep(SCROLL_PAUSE_TIME)

        src = driver.page_source
        tweets_elem = driver.find_elements_by_css_selector(selector)
        nbr_tweets_2 = len(tweets_elem)
        print nbr_repetition, "    _   ", nbr_tweets_1 , " Nombre tweets ", nbr_tweets_2 , "   ",SCROLL_PAUSE_TIME

        if nbr_tweets_2 == 0 :
            nbr_repetition = 3
            break
        elif nbr_tweets_2 > nbr_quota :
            nbr_repetition = 10
            break
        elif nbr_tweets_1 == nbr_tweets_2:
            nbr_repetition += 1
            SCROLL_PAUSE_TIME = SCROLL_PAUSE_TIME + 4

    return nbr_repetition

def extract_infos(driver, cursor, connection, count, nom, tweet_user_id, user_link, tweets_elem_nbr_item,insert_or_update):
    tweets_nbr = driver.find_elements_by_css_selector("li[class='ProfileNav-item ProfileNav-item--tweets is-active']")
    if len(tweets_nbr) == 0:
        tweets_nbr = driver.find_elements_by_css_selector("li[class='ProfileNav-item ProfileNav-item--tweets']")
    if len(tweets_nbr) > 0:
        tweets_elem_nbr = tweets_nbr[0]
        tweets_elem_nbr = tweets_elem_nbr.find_element_by_css_selector("span[class='ProfileNav-value']")
        tweets_elem_nbr_item = tweets_elem_nbr.get_attribute("data-count")
        if tweets_elem_nbr_item == None:
            tweets_elem_nbr_item = -1
            # print " nbr tweets " , tweets_elem_nbr_item

    j_aime_item = -1
    tweets_j_aimes = driver.find_elements_by_css_selector("li[class='ProfileNav-item ProfileNav-item--favorites']")
    if len(tweets_j_aimes) > 0:
        tweets_j_aimes = tweets_j_aimes[0]
        j_aime_elem = tweets_j_aimes.find_element_by_css_selector("span[class='ProfileNav-value']")
        j_aime_item = j_aime_elem.get_attribute("data-count")
        if j_aime_item == None:
            j_aime_item = -1
            # print " total  j aimes ", j_aime_item

    tweets_moments_item = -1
    tweets_moments = driver.find_elements_by_css_selector("li[class='ProfileNav-item ProfileNav-item--moments']")
    if len(tweets_moments) > 0:
        tweets_moments = tweets_moments[0]
        tweets_moments = tweets_moments.find_element_by_css_selector("span[class='ProfileNav-value']")
        tweets_moments_item = tweets_moments.get_attribute("data-count")
        if tweets_moments_item == None:
            tweets_moments_item = -1
            # print " tweets_moments ", tweets_moments_item

    abonnemenet_item = -1
    tweets_abonnemenets = driver.find_elements_by_css_selector("li[class='ProfileNav-item ProfileNav-item--following']")
    if len(tweets_abonnemenets) > 0:
        tweets_abonnemenets = tweets_abonnemenets[0]
        abonnemenets = tweets_abonnemenets.find_element_by_css_selector("span[class='ProfileNav-value']")
        abonnemenet_item = abonnemenets.get_attribute("data-count")
        if abonnemenet_item == None:
            abonnemenet_item = -1
            # print " total  abonnemenet ", abonnemenet_item

    follower_item = -1
    tweets_followers = driver.find_elements_by_css_selector("li[class='ProfileNav-item ProfileNav-item--followers']")
    if len(tweets_followers) > 0:
        tweets_followers = tweets_followers[0]
        follower = tweets_followers.find_element_by_css_selector("span[class='ProfileNav-value']")
        follower_item = follower.get_attribute("data-count")
        if follower_item == None:
            follower_item = -1
            # print " total  abonnee ", follower_item

    tweets_list_item = -1
    tweets_lists = driver.find_elements_by_css_selector("li[class='ProfileNav-item ProfileNav-item--lists']")
    if len(tweets_lists) > 0:
        tweets_lists = tweets_lists[0]
        tweets_list = tweets_lists.find_element_by_css_selector("span[class='ProfileNav-value']")
        tweets_list_item = tweets_list.get_attribute("data-count")
        if tweets_list_item == None:
            tweets_list_item = -1
            # print " total  abonnemenet ", tweets_list_item

    tweets_location_item = 'null'
    tweets_locations = driver.find_elements_by_css_selector("div[class='ProfileHeaderCard-location ']")
    if len(tweets_locations) > 0:
        tweets_location = tweets_locations[0]
        tweets_location = tweets_location.find_element_by_css_selector(
            "span[class='ProfileHeaderCard-locationText u-dir']")
        tweets_location_item = tweets_location.get_attribute("innerHTML")
        tweets_location_item = re.sub(' +', ' ', tweets_location_item)
        tweets_location_item = tweets_location_item.replace("\n", "")
        tweets_location_item = tweets_location_item.replace("'", "")
        # print " total  tweets_location ", tweets_location_item

    tweets_joinDate_item = 'null'
    tweets_joinDate = driver.find_elements_by_css_selector("div[class='ProfileHeaderCard-joinDate']")
    if len(tweets_joinDate) > 0:
        tweets_joinDate = tweets_joinDate[0]
        tweets_joinDate = tweets_joinDate.find_element_by_css_selector(
            "span[class='ProfileHeaderCard-joinDateText js-tooltip u-dir']")
        tweets_joinDate_item = tweets_joinDate.get_attribute("data-original-title")

        # print " total  tweets_joinDate ", tweets_joinDate_item

    url_page_item = 'null'
    url_page = driver.find_elements_by_css_selector("div[class='ProfileHeaderCard-url ']")
    if len(url_page) > 0:
        url_page = url_page[0]
        url_page = url_page.find_element_by_css_selector("a[class='u-textUserColor']")
        url_page_item = url_page.get_attribute("title")
        # print " profile  url_page ", url_page_item

    photo_rail_item = 'null'
    photo_rail = driver.find_elements_by_css_selector("div[class='PhotoRail-heading']")
    if len(photo_rail) > 0:
        photo_rail = photo_rail[0]
        photo_rail = photo_rail.find_element_by_css_selector("a[class='PhotoRail-headingWithCount js-nav']")
        photo_rail_item = photo_rail.get_attribute("innerHTML")
        photo_rail_item = re.sub(' +', ' ', photo_rail_item)
        photo_rail_item = photo_rail_item.replace("\n", "")
        # print " profile  photo_rail ", photo_rail_item

    birthday_item = 'null'
    birthday = driver.find_elements_by_css_selector("div[class='ProfileHeaderCard-birthdate']")
    if len(birthday) > 0:
        birthday = birthday[0]
        birthday = birthday.find_element_by_css_selector("span[class='js-tooltip']")
        birthday_item = birthday.get_attribute("innerHTML")
        # print " profile  birthday ", birthday_item

    if insert_or_update == "insert":
        ajouter_tweet_user(cursor, connection, count, nom, tweet_user_id, user_link, tweets_elem_nbr_item, j_aime_item,
                           tweets_moments_item,
                           abonnemenet_item, follower_item, tweets_list_item, tweets_location_item, tweets_joinDate_item,
                           url_page_item, photo_rail_item, birthday_item)
    elif insert_or_update == "update":
        update_user(cursor, connection, count, nom, tweet_user_id, user_link, tweets_elem_nbr_item, j_aime_item,
                           tweets_moments_item,
                           abonnemenet_item, follower_item, tweets_list_item, tweets_location_item, tweets_joinDate_item,
                           url_page_item, photo_rail_item, birthday_item)

def scroll_down_popup(driver,selector,selec2):

    SCROLL_PAUSE_TIME = 4
    count = 0

    # Get scroll height
    last_height = driver.execute_script("return document.body.scrollHeight")
    src = driver.page_source
    tweets_elem = driver.find_elements_by_css_selector(selector)
    nbr_tweets_1 = 0
    nbr_tweets_2 = len(tweets_elem)
    print nbr_tweets_1," Nombre tweets ", nbr_tweets_2
    nbr_repetition = 0

    while (nbr_repetition < 3):
        nbr_tweets_1 = nbr_tweets_2
        # Scroll down to bottom
        count += 1
        #scr1 = driver.find_element_by_xpath('/html/body/div[2]/div/div[2]/div/div[2]')
        #driver.execute_script("arguments[0].scrollTop = arguments[0].scrollHeight", tweets_elem)
        tweets_element = driver.find_element_by_xpath(selec2)
        tweets_element.send_keys(Keys.END)
        #driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        # Wait to load page
        time.sleep(SCROLL_PAUSE_TIME)

        tweets_elem = driver.find_elements_by_css_selector(selector)
        nbr_tweets_2 = len(tweets_elem)
        print nbr_repetition, "    _   ", nbr_tweets_1 , " Nombre tweets ", nbr_tweets_2 , "   ",SCROLL_PAUSE_TIME

        if nbr_tweets_1 == nbr_tweets_2:
            nbr_repetition += 1
            SCROLL_PAUSE_TIME = SCROLL_PAUSE_TIME * 2

def ajouter_tweet(cur,con,count,nom,tweet_tweet_id , tweet_item_id , tweet_user_id, tweet_tweet_type,tweet_name,
                  tweet_permalink_path,tweet_has_card,tweet_card_type,tweet_component_context,tweet_time_title,
                  tweet_text, tweet_response,tweet_retweets,tweet_j_aime,tweet_time_unix,tweet_disclosure_type,
                  tweet_nonce_nonce,tweet_stat_initialized,tweet_conversation_section,tweet_conversation_id,
                  tweet_has_parent,pays,cherch):

    sql = "INSERT INTO `" + database_name + "`.`" + nom + "` VALUES (" + str(tweet_tweet_id) + "," + str(tweet_item_id)+ "," \
          + str(tweet_user_id) +",'" + remplacer_quotes(str(tweet_tweet_type))+"','" + remplacer_quotes(str(tweet_name))+ "','" + remplacer_quotes(str(tweet_permalink_path)) \
          + "','" + str(tweet_has_card) +"','" + str(tweet_card_type) +"','"+remplacer_quotes(str(tweet_component_context))+\
          "','" + remplacer_quotes(str(tweet_time_title)) + "','"+remplacer_quotes(str(tweet_text))+"',"+str(tweet_response)+\
          ","+str(tweet_retweets)+","+str(tweet_j_aime)+","+str(tweet_time_unix)+",'"+str(tweet_disclosure_type)+"','"+\
          str(tweet_nonce_nonce)+"','"+str(tweet_stat_initialized)+"','"+str(tweet_conversation_section)+"','"+str(tweet_conversation_id)+"','"+\
          str(tweet_has_parent)+"',0,'"+pays+"','"+cherch+"',-1,-1,-1);"

    try:
        print "(", count, "_", nom, ") Sql Group : ", sql
        cur.execute(sql)
        con.commit()
    except Exception as e:
        print "(", count, "_", nom, ") Insert Tweet Exception  :  ", e

def ajout_tweet_reactor(con,cur,count,nom,tweet_id,retweeter_item_screen_name,retweeter_name,
                                retweeter_aerobasc_name,retweeter_message_content,tweet_user_id):
                sql = " INSERT INTO `" + database_name + "`.`" + nom + "`(`tweet_id`," \
                       "`reactor_screen_name`,`reactor_aerobasc`,`reactor_name`,`react_contenu`,`tweet_user_id`)" \
                       "VALUES('" + str(tweet_id) + "','" + str(retweeter_item_screen_name) + "','" + str(retweeter_name) + "'," \
                       "'" + str(retweeter_aerobasc_name) + "','" + str(retweeter_message_content) + "','" + str(tweet_user_id) + "');"
                try:
                    print "(", count, ") Sql Tweet retweeters : ", sql
                    cur.execute(sql)
                    con.commit()
                except Exception as e:
                    print "(", count, ") Insert Tweet retweeters Exception  :  ", e

def ajouter_tweet_share(cur,con,count,nom,tweet_tweet_id , tweet_item_id , tweet_user_id, tweet_tweet_type,tweet_name,
                  tweet_permalink_path,tweet_has_card,tweet_card_type,tweet_component_context,tweet_time_title,
                  tweet_text, tweet_response,tweet_retweets,tweet_j_aime,tweet_time_unix,tweet_disclosure_type,
                  tweet_nonce_nonce,tweet_stat_initialized,tweet_conversation_section,tweet_conversation_id,
                  tweet_has_parent,id_sutied):

    sql = "INSERT INTO `" + database_name + "`.`tweets_share` VALUES (" + str(tweet_tweet_id) + "," + str(tweet_item_id)+ "," \
          + str(tweet_user_id) + ",'" + remplacer_quotes(str(tweet_tweet_type))+ "','" + remplacer_quotes(str(tweet_name))+ "','" + remplacer_quotes(str(tweet_permalink_path)) \
          + "','" + str(tweet_has_card) + "','" + str(tweet_card_type) + "','"+remplacer_quotes(str(tweet_component_context))+\
          "','" + remplacer_quotes(str(tweet_time_title)) + "','"+remplacer_quotes(str(tweet_text))+"',"+str(tweet_response)+\
          ","+str(tweet_retweets)+","+str(tweet_j_aime)+","+str(tweet_time_unix)+",'"+str(tweet_disclosure_type)+"','"+\
          str(tweet_nonce_nonce)+"','"+str(tweet_stat_initialized)+"','"+str(tweet_conversation_section)+"','"+str(tweet_conversation_id)+"','"+\
          str(tweet_has_parent)+"',0,"+str(id_sutied)+");"

    try:
        print "(", count, "_", nom, ") Sql Group : ", sql
        #cur.execute(sql)
        #con.commit()
    except Exception as e:
        print "(", count, "_", nom, ") Insert Tweet Exception  :  ", e


def ajouter_tweet_user(cur, con, count, nom,tweet_user_id, user_link ,tweets_nbr,tweets_j_aimes,tweets_moments,
                                   tweets_abonnemenets,tweets_followers,tweets_lists,tweets_locations,tweets_joinDate,
                                   url_page,photo_rail,birthday):

    sql = "INSERT INTO `" + database_name + "`.`user_profile` (`user_id`,`user_link`,`tweets_nbr`,`tweets_j_aimes`," \
          "`tweets_moments`,`tweets_abonnemenets`,`tweets_followers`,`tweets_lists`,`tweets_locations`," \
          "`tweets_joinDate`,`url_page`,`photo_rail`,`birthday`)" \
          " VALUES ('"+str(tweet_user_id)+"','"+str(user_link)+"','"+str(tweets_nbr)+"','"+str(tweets_j_aimes)+"','"+str(tweets_moments)+"'," \
          "'"+str(tweets_abonnemenets)+"','"+str(tweets_followers)+"','"+str(tweets_lists)+"','"+str(tweets_locations)+"'," \
          "'"+str(tweets_joinDate)+"','"+str(url_page)+"','"+str(photo_rail)+"','"+str(birthday)+"');"

    try:
        print "(", count, "_", nom, ") Sql User : ", sql
        cur.execute(sql)
        con.commit()
    except Exception as e:
        print "(", count, "_", nom, ") Insert User Exception  :  ", e

def update_user(cur, con, count, nom,tweet_user_id, user_link ,tweets_nbr,tweets_j_aimes,tweets_moments,
                                   tweets_abonnemenets,tweets_followers,tweets_lists,tweets_locations,tweets_joinDate,
                                   url_page,photo_rail,birthday):

    sql = "update `" + database_name + "`.`user_profile` set `user_id`='"+str(tweet_user_id)+"',`user_link`='"+str(user_link)+"'," \
          "`tweets_nbr`='"+str(tweets_nbr)+"',`tweets_j_aimes`='"+str(tweets_j_aimes)+"'," \
          "`tweets_moments`='"+str(tweets_moments)+"',`tweets_abonnemenets`='"+str(tweets_abonnemenets)+"'," \
          "`tweets_followers`='"+str(tweets_followers)+"',`tweets_lists`='"+str(tweets_lists)+"'," \
          "`tweets_locations`='"+str(tweets_locations)+"'," \
          "`tweets_joinDate`='"+str(tweets_joinDate)+"',`url_page`='"+str(url_page)+"',`photo_rail`='"+str(photo_rail)+"'," \
          "`birthday`='"+str(birthday)+"'"

    try:
        print "(", count, "_", nom, ") Update user : ", sql
        cur.execute(sql)
        con.commit()
    except Exception as e:
        print "(", count, "_", nom, ") Update user Exception  :  ", e


def update_tweeter_user(cur, con, count, nom_table,champ_to_update , val_update, champ_where , val_where):

    sql = " update `" + database_name + "`.`"+nom_table+"`  set "+str(champ_to_update)+ "="+str(val_update)+ " " \
            "where "+str(champ_where)+ " = '"+str(val_where)+"' "

    try:
        print "(", count, "_", nom_table, ") Sql Update : ", sql
        cur.execute(sql)
        con.commit()
    except Exception as e:
        print "(", count, "_", nom_table, ") Insert Insertion Exception  :  ", e

def ajouter_follower_following(cur, con, count, nom_table,user_id_initial, follower_id ,follower_type,follower_id_item,follower_link,
                               follower_status,follower_src):

    sql = "INSERT INTO `" + database_name + "`.`"+nom_table+"` (`user_id_initial`,`follow_id`,`follow_type`," \
            "`follow_id_item`,`follow_link`,`follow_src`,`follow_status`)VALUES('"+str(user_id_initial)+"'," \
            "'"+str(follower_id)+"','"+str(follower_type)+"','"+str(follower_id_item)+"','"+str(follower_link)+"','"+str(follower_src)+"'," \
            "'"+str(follower_status)+"');"

    try:
        print "(", count, "_", nom_table, ") Sql Insertion : ", sql
        cur.execute(sql)
        con.commit()
    except Exception as e:
        print "(", count, "_", nom_table, ") Insert Insertion Exception  :  ", e


def remplacer_quotes(chaine):
    chaine = chaine.replace("'", "")
    chaine = chaine.replace('"', '')
    return chaine


def ecriture_fichier(nom_fic, val, lock):
    lock.acquire()
    # print lock.locked, " lock ", lock

    try:
        with open(nom_fic, 'wb') as f:
            pickle.dump(val, f)
    finally:
        lock.release()


def lecture_fichier(nom_fic, lock):
    lock.acquire()
    # print lock.locked, " lock ", lock

    try:
        with open(nom_fic, 'rb') as f:
            file = pickle.load(f)
    finally:
        lock.release()
    return file

