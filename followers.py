# -*- coding: utf-8 -*-
import time
import os
import threading

import re
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from _0_Functions import *


def tweets():
    # print lock.locked, " lock ", lock
    connection = connect_db()
    cursor = connection.cursor()

    sql_recherche = " select tweet_user_id ,  max(tweet_permalink_path) from tweets where tweet_user_id not in " \
                    " ( select user_id_initial  from followers  ) group by tweet_user_id order by tweet_user_id ASC"
    cursor.execute(sql_recherche)
    tweets_items = cursor.fetchall()
    nbr_tweets = cursor.rowcount
    print nbr_tweets, "   ", sql_recherche
    count = 0
    nom = " Twitter "
    ids = []
    follower_id_list = []
    if nbr_tweets > 0:
        driver = Acces_Webdriver()
        Authentification(driver, "", "")

    for tweet_item in tweets_items:
        user_id_initial = str(tweet_item[0])
        count += 1

        if user_id_initial not in ids:
            del follower_id_list [:]
            ids.append(user_id_initial)
            user_link = str(tweet_item[1])

            # print " 1 tweet user link  " ,user_link
            user_link = user_link.split("/status")
            user_link = user_link[0]
            # print " 2 tweet user link  ", user_link


            try:

                url = 'https://twitter.com' + user_link + "/followers"
                print nbr_tweets, "  ", count, "  ", url
                # try:
                driver.get(url)
                time.sleep(2)
                re_parcours = True
                nbr_quota = 150
                while re_parcours:
                    nbr_elements = scroll_down_follow(driver, "div[class='Grid-cell u-size1of2 u-lg-size1of3 u-mb10']",
                                                       nbr_quota)
                    if nbr_elements == 3:
                        re_parcours = False
                    print "-----------------------------------------------------------------------------"
                    nbr_quota = nbr_quota + 150
                    #scroll_down_follow(driver, "")

                    nbr_followers = 0
                    followers = driver.find_elements_by_css_selector("div[class='Grid-cell u-size1of2 u-lg-size1of3 u-mb10']")
                    for follower in followers:
                        nbr_followers += 1
                        follower_item_id = ""
                        follower_type = ""
                        follower_id = ""
                        follower_href = ""
                        follower_img_src = ""
                        follower_statu = ""

                        follower_more = follower.find_elements_by_css_selector("div[class='js-stream-item']")
                        if len(follower_more) > 0:
                            follower_more = follower_more[0]
                            follower_item_id = follower_more.get_attribute("data-item-id")
                            follower_type = follower_more.get_attribute("data-item-type")
                            follower_id = follower_more.get_attribute("id")

                        if follower_id not in follower_id_list:



                            follower_more_href = follower.find_elements_by_css_selector(
                                "a[class='ProfileCard-avatarLink js-nav js-tooltip']")
                            if len(follower_more_href) > 0:
                                follower_href = follower_more_href[0]
                                follower_href = follower_href.get_attribute("href")

                            follower_img = follower.find_elements_by_css_selector(
                                "img[class='ProfileCard-avatarImage js-action-profile-avatar']")
                            if len(follower_img) > 0:
                                follower_img = follower_img[0]
                                follower_img_src = follower_img.get_attribute("src")

                            follower_status = follower.find_elements_by_css_selector("span[class='FollowStatus']")
                            if len(follower_status) > 0:
                                follower_statu = follower_status[0]
                                follower_statu = follower_statu.get_attribute("innerHTML")

                            # print nbr_followers, " __ ",follower_item_id , " 1  ",follower_type ,"   2  ", follower_id,
                            # "  3  ",follower_href,"  4  ",follower_img_src,"   5  ",follower_statu , "  6  "

                            ajouter_follower_following(cursor, connection, nbr_followers, "followers", user_id_initial,
                                                       follower_item_id,
                                                       follower_type, follower_id, follower_href, follower_statu, follower_img_src)

                            # time.sleep(100)
                            follower_id_list.append(follower_id)


                time.sleep(5)

            except Exception as e:
                    PrintException()

            #        driver.close()
            #        driver.quit()


            # except Exception as e:
            # if "unexpectedly exited. Status code was" in e:
            #   exit()

    if nbr_tweets > 0:
        driver.close()
        driver.quit()


if __name__ == '__main__':
    tweets()
