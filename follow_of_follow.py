# -*- coding: utf-8 -*-
import time
import os
import threading

import re
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from _0_Functions import *


def fllow_of_follow(nom_table):
    # print lock.locked, " lock ", lock
    connection = connect_db()
    cursor = connection.cursor()

    sql_integration_from_followers = "insert into user_profile(user_id,user_link) " \
                                     "(select follow_id,follow_link from " + nom_table + "  " \
                                    "where follow_id not in ( select user_id from `user_profile` ) " \
                                    "order by follow_id ASC group by follow_id )"
    print  sql_integration_from_followers
    cursor.execute(sql_integration_from_followers)
    nbr_updated = cursor.rowcount
    print nbr_updated, "   ", sql_integration_from_followers
    connection.commit()

    sql_recherche = " select user_link,user_id from user_profile where user_id not in" \
                    " ( select user_id_initial from " + nom_table + " group by user_id_initial ) "
    cursor.execute(sql_recherche)
    tweets_items = cursor.fetchall()
    nbr_tweets = cursor.rowcount
    print nbr_tweets, "   ", sql_recherche
    count = 0
    nom = " Twitter "
    ids = []
    if nbr_tweets > 0:
        driver = Acces_Webdriver()
        Authentification(driver, "", "")
    following_id_list = []

    for tweet_item in tweets_items:
        user_id_initial = str(tweet_item[1])
        count += 1
        del following_id_list[:]
        if user_id_initial not in ids:
            ids.append(user_id_initial)
            user_link = str(tweet_item[0])

            # print " 1 tweet user link  " ,user_link
            user_link = user_link.split("https://twitter.com")
            user_link = user_link[1]

            url = 'https://twitter.com' + user_link + "/" + str(nom_table)
            print nbr_tweets, "  ", count, "  ", url
            # try:
            driver.get(url)
            time.sleep(2)
            tweets_elem_nbr_item = -1
            extract_infos(driver, cursor, connection, count, nom, user_id_initial, user_link,
                          tweets_elem_nbr_item, "update")

            re_parcours = True
            nbr_quota = 150
            while re_parcours:
                nbr_elements = scroll_down_follow(driver,
                                                  "div[class='Grid-cell u-size1of2 u-lg-size1of3 u-mb10']",
                                                  nbr_quota)
                if nbr_elements == 3:
                    re_parcours = False
                print "-----------------------------------------------------------------------------"
                nbr_quota = nbr_quota + 150

            nbr_followers = 0
            followers = driver.find_elements_by_css_selector("div[class='Grid-cell u-size1of2 u-lg-size1of3 u-mb10']")
            for follower in followers:
                nbr_followers += 1
                follower_item_id = ""
                follower_type = ""
                follower_id = ""
                follower_href = ""
                follower_img_src = ""
                follower_statu = ""

                follower_more = follower.find_elements_by_css_selector("div[class='js-stream-item']")
                if len(follower_more) > 0:
                    follower_more = follower_more[0]
                    follower_item_id = follower_more.get_attribute("data-item-id")
                    follower_type = follower_more.get_attribute("data-item-type")
                    follower_id = follower_more.get_attribute("id")

                if follower_id not in following_id_list:

                    follower_more_href = follower.find_elements_by_css_selector(
                        "a[class='ProfileCard-avatarLink js-nav js-tooltip']")
                    if len(follower_more_href) > 0:
                        follower_href = follower_more_href[0]
                        follower_href = follower_href.get_attribute("href")

                    follower_img = follower.find_elements_by_css_selector(
                        "img[class='ProfileCard-avatarImage js-action-profile-avatar']")
                    if len(follower_img) > 0:
                        follower_img = follower_img[0]
                        follower_img_src = follower_img.get_attribute("src")

                    follower_status = follower.find_elements_by_css_selector("span[class='FollowStatus']")
                    if len(follower_status) > 0:
                        follower_statu = follower_status[0]
                        follower_statu = follower_statu.get_attribute("innerHTML")

                    # print nbr_followers, " __ ",follower_item_id , " 1  ",follower_type ,"   2  ", follower_id,
                    # "  3  ",follower_href,"  4  ",follower_img_src,"   5  ",follower_statu , "  6  "

                    ajouter_follower_following(cursor, connection, nbr_followers, nom_table, user_id_initial,
                                               follower_item_id,
                                               follower_type, follower_id, follower_href, follower_statu,
                                               follower_img_src)
                    following_id_list.append(follower_id)
                    # time.sleep(100)

            time.sleep(5)

            # except Exception as e:
            #        print " ID introuvable :  ", e
            #        driver.close()
            #        driver.quit()


            # except Exception as e:
            # if "unexpectedly exited. Status code was" in e:
            #   exit()

    if nbr_tweets > 0:
        driver.close()
        driver.quit()


if __name__ == '__main__':
    fllow_of_follow("followers")
    fllow_of_follow("following")
