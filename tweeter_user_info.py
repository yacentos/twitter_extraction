# -*- coding: utf-8 -*-
import time
import os
import threading

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from _0_Functions import *


def tweets():
    # print lock.locked, " lock ", lock
    connection = connect_db()
    cursor = connection.cursor()

    sql_recherche = " select tweet_user_id  , max(tweet_permalink_path) from tweets where tweet_user_id not in " \
                    " ( select user_id  from user_profile ) group by tweet_user_id order by tweet_user_id DESC "
    cursor.execute(sql_recherche)
    tweets_items = cursor.fetchall()
    nbr_tweets = cursor.rowcount
    count = 0
    nom = " Twitter "
    ids = []
    driver = Acces_Webdriver()

    for tweet_item in tweets_items:
        tweet_user_id = str(tweet_item[0])
        count += 1
        if tweet_user_id not in ids:
            ids.append(tweet_user_id)
            user_link = str(tweet_item[1])

            # print " 1 tweet user link  " ,user_link
            user_link = user_link.split("/status")
            user_link = user_link[0]
            # print " 2 tweet user link  ", user_link


            try:

                url = 'https://twitter.com' + user_link
                print nbr_tweets, "  ", count, "  ", url
                try:
                    driver.get(url)
                    time.sleep(1)
                    tweets_elem_nbr_item = -1
                    extract_infos(driver, cursor, connection, count, nom, tweet_user_id, user_link,
                                  tweets_elem_nbr_item, "insert")
                    time.sleep(1)

                except Exception as e:
                    PrintException()
                    driver.close()
                    driver.quit()


            except Exception as e:
                PrintException()

    driver.close()
    driver.quit()


if __name__ == '__main__':
    tweets()
